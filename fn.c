#include <fnmatch.h>
#include <stdlib.h>
int main (int argc, char *argv[]) {
/* FNM_PATHNAME    = 1
 * FNM_NOESCAPE    = 2
 * FNM_PERIOD      = 4
 * FNM_LEADING_DIR = 8
 * FNM_CASEFOLD    = 16 */
  return argc > 2 ? fnmatch(argv[1],argv[2],argc > 3 ? atoi(argv[3]) : 0) : 1;
}